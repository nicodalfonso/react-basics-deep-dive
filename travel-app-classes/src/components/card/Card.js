import React from "react"
import "./Card.css"
import Title from "../title/Title"

const Card = (props) => {
    return(
        <div className="card">
            <Title name={props.name} location={props.location}/>
            <div>Description</div>
            <div>
                <ul>
                <li>Address</li>
                <li>Hours</li>
                <li>Contact</li>
                </ul>
            </div>
        </div>
    )
}

export default Card;