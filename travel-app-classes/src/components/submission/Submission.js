import React, {Component} from "react"

const defaultState = {
    name: "",
    location: "",
    count: 0,
}
const otherState = {...defaultState}

class Submission extends Component {
    state = {
        name: "",
        location: "",
    }
    setName = (value) => this.setState({name: value})
    setLocation = (value) => this.setState({location: value})

    submitForm = (event) => {
        event.preventDefault();
        otherState.count += 1
        console.log(otherState, defaultState)
       this.setState({...defaultState, count: otherState.count})
    }

    handleChange = (event) => {
        console.log(event.target)
        this.setState({[event.target.name]: event.target.value})
    }

    render() {

        return (
            <form onSubmit={this.submitForm}>
                <h5>You've contributed {this.state.count} destinations</h5>
                <label htmlFor="name">Name:</label>
                <input name="name" value={this.state.name} onChange={this.handleChange}/>
                <label htmlFor="location">Location:</label>
                <input name="location" value={this.state.location} onChange={this.handleChange}/>
                <button type="submit">Add to list</button>
            </form>
        )
    }
}

export default Submission;