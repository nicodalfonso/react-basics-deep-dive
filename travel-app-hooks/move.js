let numbers = [45, 23, 13, 10];

// ----------------------------------------------------------
// First Solution: Pushing & Slicing
// const insert = (arr, from, to) => {
//   const itemToMove = arr[from];
//   const nextArr = [];

//   arr.forEach((el, i) => {
//     if (i != from) {
//       nextArr.push(el);
//     }
//   });
//   console.log(`during: ${nextArr}`);

//   return [
//     // part of the array before the specified index
//     ...nextArr.slice(0, to),
//     // inserted item
//     itemToMove,
//     // part of the array after the specified index
//     ...nextArr.slice(to),
//   ];
// };

// console.log(`First Solution: Pushing & Slicing`);
// console.log(`before: ${numbers}`);
// console.log(`after: ${insert(numbers, 3, 0)}\n`);

// ----------------------------------------------------------
// Second Solution: Boolean Arithmetic
// const reorder = (arr, from, to) => {
//   nextArr = [...arr];

//   // insert copy of element in appropriate position
//   // False == 0, True == 1
//   nextArr.splice(to + (to > from), 0, arr[from]);
//   console.log(`during: ${nextArr}`);

//   // remove element from original position
//   // False == 0, True == 1
//   nextArr.splice(from + (to < from), 1);

//   console.log(`index to remove: ${from + (to < from)}`);
//   return nextArr;
// };

// console.log(`Second Solution: Boolean Arithmetic`);
// console.log(`before: ${numbers}`);
// console.log(`after: ${reorder(numbers, 3, 0)}\n`);

// ----------------------------------------------------------
// // Third Solution: Nested Splicing
// const moveFromTo = (arr, from, to) => {
//   const next = [...arr];

//   // Insert an item into next at the 'to' index
//   // What's getting inserted?:
//   // The item returned from splicing at the 'from' index
//   next.splice(to, 0, next.splice(from, 1)[0]);
//   return next;
// };

// console.log(`Third Solution: `);
// console.log(`before: ${numbers}`);
// console.log(`after: ${moveFromTo(numbers, 3, 0)}\n`);

// ----------------------------------------------------------
// Fourth Solution: Ternary Indexing
// function anotherMoveFromTo(arr, from, to) {
//   const nextIndex = to >= arr.length ? arr.length - 1 : to;
//   // Insert an item into next at the 'nextIndex' index
//   // What's getting inserted?:
//   // The item returned from splicing at the 'from' index
//   arr.splice(nextIndex, 0, arr.splice(from, 1)[0]);
//   return arr;
// }

// console.log(`Fourth Solution: `);
// console.log(`before: ${numbers}`);
// console.log(`after: ${anotherMoveFromTo(numbers, 3, 0)}`);
