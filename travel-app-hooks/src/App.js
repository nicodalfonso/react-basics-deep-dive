import React from "react";
import { useDisclosure, Stack, Text } from "@chakra-ui/react";
import { Switch, Route } from "react-router-dom";
import "./App.css";
import { NavBar, Signup, NotFound } from "./components";
import { HomeScreen, DocsScreen, ExamplesScreen, BlogScreen } from "./screens/";

function App() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <NavBar isOpen={isOpen} onOpen={onOpen} onClose={onClose} />
      <Stack>
        <Switch>
          <Route exact path="/" component={HomeScreen} />
          <Route path="/docs/examples" component={ExamplesScreen} />
          <Route path="/docs" component={DocsScreen} />
          <Route path="/blog" component={BlogScreen} />
          <Route path="/signup" component={HomeScreen} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Stack>
      <Signup isOpen={isOpen} onClose={onClose} />

      <Stack direction="row" bg="gray.400" h={10} color="white" alignItems="center" justify="center">
        <Text>Footer</Text>
      </Stack>
    </>
  );
}

export default App;
