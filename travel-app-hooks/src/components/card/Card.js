import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Stack, Button, Select, Heading, ListItem, UnorderedList } from "@chakra-ui/react";
import { Title } from "../";
import { deleteCard, reorderCards } from "../../store/actions";

export const Card = ({ data: { name, location, index, id } }) => {
  const submissions = useSelector((state) => state.submissions);
  const dispatch = useDispatch();

  const deleteSelf = () => {
    dispatch(deleteCard(id));
  };

  const handleChange = (e) => {
    dispatch(reorderCards({ id, choice: Number.parseInt(e.target.value, 10) - 1 }));
  };

  return (
    <Stack
      align="center"
      className="card"
      bg="blue.300"
      minW={290}
      minH={400}
      color="white"
      spacing="24px"
      borderRadius={10}
      border="2px solid gray"
      boxShadow="2px 2px 5px 2px gray"
      p={6}
    >
      <Stack direction="row" justify="space-between" w="100%">
        <Select value={index + 1} onChange={(e) => handleChange(e)} w={20}>
          {submissions.map((el, i) => (
            <option value={i + 1} key={el.id + "option"}>
              {i + 1}
            </option>
          ))}
        </Select>
        <Button alignSelf="flex-end" colorScheme="red" size="sm" w={10} onClick={deleteSelf}>
          X
        </Button>
      </Stack>
      <Title name={name} location={location} />
      <Heading as="h4" size="md">
        Description
      </Heading>
      <UnorderedList>
        <ListItem>Address</ListItem>
        <ListItem>Hours</ListItem>
        <ListItem>Contact</ListItem>
      </UnorderedList>
    </Stack>
  );
};
