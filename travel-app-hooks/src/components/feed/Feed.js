import React from "react";
import { Stack, Divider } from "@chakra-ui/react";
import { Card } from "../";
import { useSelector } from "react-redux";

export const Feed = () => {
  const submissions = useSelector((state) => state.submissions);
  return (
    <Stack direction="row" w="100vw" h={480} alignItems="center" spacing={4} overflow="scroll">
      {submissions.map((el, i) => {
        const feedData = {
          name: el.name,
          location: el.location,
          index: i,
          id: el.id,
        };
        return (
          <>
            <Card data={feedData} key={el.id} />
            <Divider orientation="vertical" h={150} />
          </>
        );
      })}
    </Stack>
  );
};
