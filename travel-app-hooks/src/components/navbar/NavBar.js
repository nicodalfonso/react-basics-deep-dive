import React from "react";
import { Link, Route } from "react-router-dom";
import { Box, Heading, Flex, Text, Button } from "@chakra-ui/react";

const MenuItems = ({ children, destination }) => (
  <Link to={destination}>
    <Text mt={{ base: 4, md: 0 }} mr={6} display="block">
      {children}
    </Text>
  </Link>
);

// Note: This code could be better, so I'd recommend you to understand how I solved and you could write yours better :)
export const NavBar = (props) => {
  const [show, setShow] = React.useState(false);
  const handleToggle = () => setShow(!show);

  return (
    <Flex as="nav" align="center" justify="space-between" wrap="wrap" padding="1.5rem" bg="blue.500" color="white" {...props}>
      <Flex align="center" mr={5}>
        <Heading as="h1" size="lg" letterSpacing={"-.1rem"}>
          <Link to="/">Travel App</Link>
        </Heading>
      </Flex>

      <Box display={{ base: "block", md: "none" }} onClick={handleToggle}>
        <svg fill="white" width="12px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
          <title>Menu</title>
          <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
        </svg>
      </Box>

      <Box display={{ sm: show ? "block" : "none", md: "flex" }} width={{ sm: "full", md: "auto" }} alignItems="center" flexGrow={1}>
        <MenuItems destination="/docs">Docs</MenuItems>
        <MenuItems destination="/docs/examples">Examples</MenuItems>
        <MenuItems destination="/blog">Blog</MenuItems>
      </Box>

      <Route exact path="/">
        <Box display={{ sm: show ? "block" : "none", md: "block" }} mt={{ base: 4, md: 0 }}>
          <Link to="/signup">
            <Button bg="transparent" border="1px" onClick={props.onOpen}>
              Create account
            </Button>
          </Link>
        </Box>
      </Route>
    </Flex>
  );
};
