import React from "react";
import { Header, Center, Heading } from "@chakra-ui/react";

export const NotFound = () => {
  return (
    <Center bg="tomato" w={500} h={500}>
      <Heading as="h1" size="4xl">
        You're Lost. Go Away.
      </Heading>
    </Center>
  );
};
