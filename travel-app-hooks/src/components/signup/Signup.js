import React from "react";
import { Link } from "react-router-dom";
import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter, Button, useDisclosure } from "@chakra-ui/react";

export const Signup = ({ isOpen, onOpen, onClose }) => {
  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Modal Title</ModalHeader>
          <Link to="/">
            <ModalCloseButton />
          </Link>
          <ModalBody>Sign up</ModalBody>

          <ModalFooter>
            <Link to="/">
              <Button colorScheme="blue" mr={3} onClick={onClose}>
                Close
              </Button>
            </Link>
            <Link to="/docs">
              <Button colorScheme="purple" mr={3} onClick={onClose}>
                Show Me The Docs!
              </Button>
            </Link>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};
