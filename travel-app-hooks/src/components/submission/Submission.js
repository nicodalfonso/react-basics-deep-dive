import React, { useState } from "react";
import { v4 } from "uuid";
import { Button, Input, Heading, FormLabel, Center, InputGroup, InputLeftAddon } from "@chakra-ui/react";
import { addCard } from "../../store/actions";
import { useDispatch, useSelector } from "react-redux";

export const Submission = ({ data: { submissions, setSubmissions } }) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Submitting!");
    const id = v4();
    dispatch(addCard({ name, location, id }));
    console.log(state);
    setName("");
    setLocation("");
  };

  return (
    <>
      <Heading as="h2" size="4xl" color="#FFFC">
        Where Do You Want To Go?
      </Heading>
      <Center border="3px solid black" mb={6} bgColor="#FFF7" borderRadius={20} m="40px 80px" p={6}>
        <form onSubmit={handleSubmit}>
          {/* <FormLabel htmlFor="name" fontWeight="900">
            Name:
          </FormLabel> */}
          <InputGroup>
            <InputLeftAddon children="Name" w={24} bg="blue.300" color="white" fontWeight="800" />
            <Input isRequired={true} variant="filled" name="name" value={name} onChange={(event) => setName(event.target.value)} />
          </InputGroup>
          {/* <FormLabel htmlFor="location" fontWeight="900">
            Location:
          </FormLabel> */}
          <InputGroup mb={4}>
            <InputLeftAddon children="Location" w={24} bg="blue.300" color="white" fontWeight="800" />
            <Input isRequired={true} variant="filled" name="location" value={location} onChange={(event) => setLocation(event.target.value)} />
          </InputGroup>
          <Button type="submit" colorScheme="blue" size="md">
            Add to list
          </Button>
        </form>
      </Center>
    </>
  );
};
