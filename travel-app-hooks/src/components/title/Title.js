import React from "react";
import { Heading, Stack } from "@chakra-ui/react";

export const Title = (props) => {
  return (
    <Stack align="center">
      <Heading as="h2" size="xl">
        {props.name}
      </Heading>
      <Heading as="h3" size="lg">
        {props.location}
      </Heading>
    </Stack>
  );
};
