import React from "react";
import { Heading } from "@chakra-ui/react";

export const BlogScreen = () => {
  return (
    <div>
      <Heading as="h1" size="3xl">
        blog
      </Heading>
    </div>
  );
};
