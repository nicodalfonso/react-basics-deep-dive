import React from "react";
import { Heading } from "@chakra-ui/react";

export const DocsScreen = () => {
  return (
    <Heading as="h1" size="3xl">
      docs
    </Heading>
  );
};
