import React from "react";
import { Heading } from "@chakra-ui/react";

export const ExamplesScreen = () => {
  return (
    <Heading as="h1" size="3xl">
      examples
    </Heading>
  );
};
