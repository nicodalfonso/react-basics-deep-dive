import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Button, Flex, Box } from "@chakra-ui/react";
import { Submission, Feed } from "../../components";
import { deleteAll } from "../../store/actions";
import nature from "../../assets/img/nature.jpg";

export const HomeScreen = () => {
  const dispatch = useDispatch();
  const [submissions, setSubmissions] = useState([]);

  const submissionData = {
    submissions: submissions,
    setSubmissions: setSubmissions,
  };

  return (
    <>
      {/* <Image src={nature} mb={4} /> */}
      <Box bgImage={`url(${nature})`} h="50%" bgPos="center" bgRepeat="no-repeat" bgSize="cover" pos="relative">
        <Box pos="relative" textAlign="center" top="50%" left="50%" transform="translate(-50%)">
          <Feed mb={4} />
          <Submission data={submissionData} />
          <Flex justify="flex-end" p={6}>
            <Button variant="outline" color="white" size="lg" onClick={() => dispatch(deleteAll())}>
              Delete All
            </Button>
          </Flex>
        </Box>
      </Box>
    </>
  );
};
