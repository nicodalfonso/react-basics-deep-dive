//ACTION TYPES
const ADD_CARD = "ADD_CARD";
const REORDER_CARDS = "REORDER_CARDS";
const DELETE_CARD = "DELETE_CARD";
const DELETE_ALL = "DELETE_ALL";

export { ADD_CARD, REORDER_CARDS, DELETE_CARD, DELETE_ALL };

//ACTION CREATORS
export const addCard = ({ name, location, id }) => {
  console.log(`I'm adding ${name}, ${location} to the store!`);
  return {
    type: ADD_CARD,
    payload: { name, location, id },
  };
};

export const reorderCards = ({ id, choice }) => {
  console.log("I'm re-ordering the cards!");
  console.log(`${id} ${choice}`);
  return {
    type: REORDER_CARDS,
    payload: { id, choice },
  };
};

export const deleteCard = (id) => {
  console.log("I'm deleting a card!");
  console.log("Action Creator id" + id);
  return {
    type: DELETE_CARD,
    payload: id,
  };
};

export const deleteAll = () => {
  console.log("I am deleteing all cards!");
  return {
    type: DELETE_ALL,
  };
};
