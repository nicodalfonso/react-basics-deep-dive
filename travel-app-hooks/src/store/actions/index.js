// /*
// WHAT ARE OUR ACTIONS?

// Inputting a new place
// Rearrange the order of places
// Delete a single place
// Delete all places

// */

// // const SOMETHING_ELSE = "PUMP_GAS"

// // console.log("DESCRIPTIVE_NAME")
// // console.log(SOMETHING_ELSE)

// //ACTION TYPES
// const ADD_CARD = "ADD_CARD";
// const REORDER_CARDS = "REORDER_CARDS";
// const DELETE_CARD = "DELETE_CARD";
// const DELETE_ALL = "DELETE_ALL";

// //ACTION CREATORS
// const addCard = () => {
//   console.log("I'm adding a card to the store!");
//   return {
//     type: ADD_CARD,
//   };
// };
// const reorderCards = () => {
//   console.log("I'm re-ordering the cards!");
//   return {
//     type: REORDER_CARDS,
//   };
// };
// const deleteCard = () => {
//   console.log("I'm deleting a card!");
//   return {
//     type: DELETE_CARD,
//   };
// };
// const deleteAll = () => {
//   console.log("Nuke it!");
//   return {
//     type: DELETE_ALL,
//   };
// };

// const makeChange = () => {
//     weCanMakeChange = checkTheDrawer();
//     if(weCanMakeChange){
//         return {
//             type: MAKE_CHANGE
//         }
//     } else {
//         return {
//             type: NO_CHANGE
//         }
//     }
// }

export * from "./cards";
