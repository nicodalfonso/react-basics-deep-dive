/*
WHAT IS OUR STATE?

Name
Location
Submissions - A list of name/location pairs
Choice - order of the name/location pairs in submissions

*/
import { createStore } from "redux";
import { cardsReducer } from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";
export const store = createStore(cardsReducer, composeWithDevTools());
