import { ADD_CARD, REORDER_CARDS, DELETE_CARD, DELETE_ALL } from "../actions";

const initialState = {
  submissions: [],
};

const insert = (arr, to, from) => {
  const nextItem = arr[from];
  const nextArr = [];
  arr.forEach((el, i) => {
    if (i != from) {
      nextArr.push(el);
    }
  });
  return [
    // part of the array before the specified index
    ...nextArr.slice(0, to),
    // inserted item
    nextItem,
    // part of the array after the specified index
    ...nextArr.slice(to),
  ];
};

export const cardsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_CARD:
      // const { name, location } = payload;
      //whatever we want to happen
      console.log(`${type} is ADD_CARD`);
      console.log(`The payload is ${payload.name} and ${payload.location}`);
      return {
        ...state,
        submissions: [...state.submissions, { ...payload }],
      };
    case REORDER_CARDS:
      const currentCardIndex = state.submissions.findIndex((card) => card.id === payload.id);
      console.log(`${type} is REORDER_CARDS`);
      console.log(`The id: ${payload.id}
        The choice: ${payload.choice}`);
      return {
        ...state,
        submissions: insert(state.submissions, payload.choice, currentCardIndex),
      };

    // state.submissions.findIndex(card => card.id === payload.id)
    // payload.choice

    case DELETE_CARD:
      console.log(`${type} is DELETE_CARD`);
      console.log(`${payload} is id`);
      return {
        ...state,
        submissions: state.submissions.filter((card) => card.id !== payload),
      };
    case DELETE_ALL:
      console.log(`${type} is DELETE_ALL`);
      return {
        ...state,
        submissions: [],
      };
    default:
      return state;
  }
};
